<?php
        $file = readline('Ingrese el nombre (sin extensión) del archivo entrada: ');
        $content = file_get_contents("$file.txt");
        $values = explode(PHP_EOL, $content);
        // Declare and init vars 
        $charCount = explode(' ' ,$values[0]);
        $firstSentence = $values[1];
        $secondSentence = $values[2];
        $hash = $values[3];
        $content = ''; // Declare content var
        ////

        preg_match_all('/[a-zA-Z0-9]/', $hash, $matches);
    
        if((strlen($hash) > 3 && strlen($hash) < 5000) && $charCount[2] == strlen($hash)) {
            if(count($matches[0]) == strlen($hash)) {
                if((strlen($firstSentence) > 3 && strlen($firstSentence) < 5000) && $charCount[0] == strlen($firstSentence)) {
                    if((strlen($secondSentence) > 3 && strlen($secondSentence) < 5000) && $charCount[1] == strlen($secondSentence)) {
                        // Process of decode message
                        $hashArray = str_split(strtolower($hash)); // Turning hash string into array
                        $lastElement = ''; // Declare lastchar variable for checking dupe case
                        foreach($hashArray as $key => $eachHash) {
                            if($eachHash == $lastElement) unset($hashArray[$key]); // Remove element from array incase dupe
                            $lastElement = $eachHash; // Save the last char for comparison
                        }
                        $newHash = implode($hashArray); // Turnig new hash array into string
                    
                        $levelComparison1 = similar_text($firstSentence, $newHash); // Checking similarity percentage from first message
                        $levelComparison2 = similar_text($secondSentence, $newHash); // Checking similarity percentage from second message
                
                        if($levelComparison1 > $levelComparison2) { // Decode process incase first message has more similarity percentage
                            $index = strpos($newHash, strtolower(substr($firstSentence, 0, 2))); // Getting index of first char in message on new hash
                            $wordInHash = substr($newHash, $index, $charCount[0]); // Getting the message by last index getted and count of chars from message
                            if($wordInHash == strtolower($firstSentence)) { // If result was the same set the response to first message YES and second NO
                                $content = "SI\nNO";
                            } else { // Incase does not match, set the response as noone message is in hash
                                $content = "NO\nNO";
                            }
                        } else { // Decode process incase second message has more similarity percentage
                            $index = strpos($newHash, strtolower(substr($secondSentence, 0, 21))); // Getting index of first char in message on new hash
                            $wordInHash = substr($newHash, $index, $charCount[1]); // Getting the message by last index getted and count of chars from message
                            if($wordInHash == strtolower($secondSentence)) { // If result was the same set the response to second message YES and first NO
                                $content = "NO\nSI";
                            } else { // Incase does not match, set the response as noone message is in hash
                                $content = "NO\nNO";
                            }
                        }
                        ////
                        // Creating file of response
                        $path = './salida.txt'; // Path and name to save
                        $fp = fopen($path, "wb"); // Open for write content
                        fwrite($fp, $content); // Write content
                        fclose($fp); // Close file
                        ////
                    
                        echo json_encode(['status' => 'done']); // Return the results in response
                    } else {
                        echo json_encode(['status' => 'fail', 'message' => 'M2 no cumple con el rango de longitud(3-50) o la longitud de la 2° instrucción es diferente a la indicada']); // Return the results incase wrong data
                    }
                } else {
                    echo json_encode(['status' => 'fail', 'message' => 'M1 no cumple con el rango de longitud(3-50) o la longitud de la 1° instrucción es diferente a la indicada']); // Return the results incase wrong data
                }
            } else {
                echo json_encode(['status' => 'fail', 'message' => 'N no cumple con los caracteres permitidos: [a-zA-Z0-9].']); // Return the results incase wrong data
            }
        } else {
            echo json_encode(['status' => 'fail', 'message' => 'N no cumple con el rango de longitud(3-50) o la longitud del mensaje es diferente a la indicada']); // Return the results incase wrong data
        }
?>