file = input('Ingresa el nombre (sin extensión) del archivo de entrada: ')
with open(f"{file}.txt") as f:
    lines = f.readlines()

rounds = lines[0]
del lines[0]

scores1 = []
scores2 = []

if int(rounds) <= 1000:
    for line in lines:
        data = line.split(' ')
        scores1.append(data[0].replace('\n', ''))
        scores2.append(data[1].replace('\n', ''))


    lastMaxScore = 0
    winner = 0

    for score1 in enumerate(scores1):
        score2 = scores2[score1[0]]
        valueScore1 = score1[1]
        if int(valueScore1) > int(score2):
            diff = int(valueScore1)-int(score2)
            if lastMaxScore < diff:
                lastMaxScore = diff
                winner = 1
        else:
            diff = int(score2)-int(valueScore1)
            if lastMaxScore < diff:
                lastMaxScore = diff
                winner = 2

    with open('salida.txt', 'w') as fw:
        fw.write(f"{winner} {lastMaxScore}")

    print('status : done')
else:
    print('status : failed, message : Número de rondas no cumple con el rango de longitud(0-1000)')