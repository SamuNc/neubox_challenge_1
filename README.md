# Neubox Challenge 1

## ¿Cómo enviar el archivo de entrada?
Estas instrucciones son para los 2 problemas y para el desarrollado en python:

1- Se coloca el archivo en la carpeta contenedora del script, ejemplo:
./Ejercicio1/EX1.php
./Ejercicio1/nombreArchivoEntrada.txt

2- Al ejecutar el script mediante la consola se soilicita el nombre del archivo de entrada sin extensión, por ejemplo:
cmd> php ./EX1.php
PHP: Ingresa el nombre (sin extensión) del archivo de entrada: 
PHP: Ingresa el nombre (sin extensión) del archivo de entrada: nombreArchivoEntrada
PHP: Execution process...

otro ejemplo con python:
cmd> py ./main.py
py: Ingresa el nombre (sin extensión) del archivo de entrada: 
py: Ingresa el nombre (sin extensión) del archivo de entrada: nombreArchivoEntrada
py: Execution process...


## Problema 1: mensaje codificado

Se resolvió con PHP nativo, se realizaron pruebas con otros datos y el resultado cumple con lo requerido

## Problema 2: ganador por ventaja

Se resolvió con PHP nativo, se realizaron pruebas con otros datos y el resultado cumple con lo requerido

## Problema 2: ganador por ventaja

Se resolvió con Python nativo, se realizaron pruebas con otros datos y el resultado cumple con lo requerido
